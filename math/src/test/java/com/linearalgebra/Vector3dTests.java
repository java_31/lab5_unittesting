/*Kayci Nicole Davila
 * student id:2141560
 */
package com.linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests {
    /*Testing vector results from Vector3d.java */
    private static Vector3d vector = new Vector3d (1.0, 2.0, 3.0);

    @Test
    public void getMethodTest(){
        assertEquals(1.0, vector.getX(),0);
        assertEquals(2.0, vector.getY(),0);
        assertEquals(3.0, vector.getZ(),0);
    }

    @Test
    public void magnitudeTest(){
        assertEquals(3.74, vector.magnitude(), 0.10);
    }

    @Test
    public void dotProductTest(){
        Vector3d vector2 = new Vector3d(4.0, 5.0, 6.0);
        assertEquals(32, vector.dotProduct(vector2), 0);
    }

    @Test
    public void addTest(){
        Vector3d vector2 = new Vector3d(4.0, 5.0, 6.0);
        assertEquals(5, vector.add(vector2).getX(), 0);
        assertEquals(7, vector.add(vector2).getY(), 0);
        assertEquals(9, vector.add(vector2).getZ(), 0);

    }

    
}
