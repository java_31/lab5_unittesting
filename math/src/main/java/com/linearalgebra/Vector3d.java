/*Kayci Nicole Davila
 * student id:2141560
 */
package com.linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

//constructor
    public Vector3d(double x,double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
//getter methods
    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double magnitude = Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)+Math.pow(this.z,2));
        return magnitude; 
    }
    
    public double dotProduct (Vector3d vector2){
        double product = this.x*vector2.getX();
        product += this.y*vector2.getY();
        product += this.z*vector2.getZ();
        return product;
    }

    public Vector3d add(Vector3d vector){
        double newX = this.x + vector.getX();
        double newY = this.y + vector.getY();
        double newZ = this.z + vector.getZ();

        Vector3d addedVector = new Vector3d(newX,newY,newZ);

        return addedVector;

    }
}
